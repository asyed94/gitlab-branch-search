#!/usr/bin/env node
import './env';
import {Gitlab} from '@gitbeaker/node';

console.log({
  host: process.env.GITLAB_HOST,
  token: process.env.GITLAB_TOKEN ? 'found' : 'not found',
});

const api = new Gitlab({
  host: process.env.GITLAB_HOST,
  token: process.env.GITLAB_TOKEN,
});

async function getSubgroups(group: string): Promise<string[]> {
  const subgroups = (await api.Groups.subgroups(group)) as unknown as Array<{
    full_path: string;
  }>;
  return subgroups.map(subgroup => subgroup.full_path);
}

async function searchGroup(group: string, term: RegExp, level = 0) {
  let printedGroup = false;
  const projects = await api.Groups.projects(group);
  for (const project of projects) {
    let printedProject = false;
    const branches = await api.Branches.all(project.id);
    for (const branch of branches) {
      if (term.test(branch.name)) {
        if (!printedGroup) {
          console.log(`${'  '.repeat(level)}${group}`);
          printedGroup = true;
        }
        if (!printedProject) {
          console.log(`${'  '.repeat(level + 1)}${project.name}`);
          printedProject = true;
        }
        console.log(`${'  '.repeat(level + 2)}* ${branch.name}`);
      }
    }
  }

  const subgroups = await getSubgroups(group);
  for (const subgroup of subgroups) {
    await searchGroup(subgroup, term, level + 1);
  }
}

async function main() {
  const group = process.argv[2];
  if (!group) throw new Error('No Gitlab group given.');
  const term = new RegExp(process.argv[3] || '.+');
  searchGroup(group, term);
}

main();
