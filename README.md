# gitlab-branch-search

A CLI tool to recursively search Gitlab groups for projects containing certain branch names.

## Install

```
$ npm install -g gitlab-branch-search
```

## Usage

### With a `.env` File

Create a `.env` file in the current working directory and set the following environment variables:

```ini
GITLAB_HOST=<GITLAB_HOST_URL>
GITLAB_TOKEN=<PERSONAL_GITLAB_TOKEN>
```

Then run the command:

```
gitlab-branch-search <FULL_GROUP_NAME> [BRANCH_SEARCH_REGEX]
```

### Powershell One-Liner

```powershell
$env:GITLAB_HOST='<GITLAB_HOST_URL>'; $env:GITLAB_TOKEN='<PERSONAL_GITLAB_TOKEN>'; gitlab-branch-search <FULL_GROUP_NAME> [BRANCH_SEARCH_REGEX]; Remove-Item Env:\GITLAB_HOST; Remove-Item Env:\GITLAB_TOKEN
```

### Bash (and compatible) One-Liner

```bash
GITLAB_HOST=<GITLAB_HOST_URL>; GITLAB_TOKEN=<PERSONAL_GITLAB_TOKEN>; gitlab-branch-search <FULL_GROUP_NAME> [BRANCH_SEARCH_REGEX]
```